package ex1;

public class Main {
    public static void main(String[] args) {
        YThread1 t1 = new YThread1();
        YThread2 t2 = new YThread2();
        t1.start();
        t2.start();

    }

}

class YThread1 extends Thread {

    public void run() {
        for (int i = 0; i < 4; i++) {
            System.out.println(Thread.currentThread().getName() + " " + System.currentTimeMillis() + " " + i + 1);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class YThread2 extends Thread {

    public void run() {
        for (int i = 0; i < 4; i++) {
            System.out.println(Thread.currentThread().getName() + " " + System.currentTimeMillis() + " " + i + 1);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
