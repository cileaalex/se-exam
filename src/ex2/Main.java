package ex2;

class I implements U {
    private long t;
    N n;

    public I(N n) {
        this.n = n;
    }

    public void f() {

    }

    public void i(J j) {

    }
}

class J {

}

interface U {

}

class K {
    L l;
    S s;
    I i;

    public K(S s, I i) {
        this.l = new L();
        this.s = s;
        this.i = i;
    }
}

class L {
    public void metA() {

    }
}

class S {
    public void metB() {

    }
}

class N {

}
